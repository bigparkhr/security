package com.bg.Security.service;

import com.bg.Security.lib.CommonFile;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

@Service
public class TempService {

    public static void main(String[] args) {
        try {
            imageResize();
        } catch (IOException e) {
            e.printStackTrace();
            e.getMessage();
        }
    }

    public void setImageFile(MultipartFile multipartFile) throws IOException {
        File file = CommonFile.multiparttoFile(multipartFile);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

        bufferedReader.close();
    }

    // 파일 정보와 리사이즈 값 정하는 메소드
    public static void imageResize() throws IOException {
        File file = new File("java.io.tmpdir"); //리사이즈할 파일 경로
        InputStream inputStream = new FileInputStream(file);
        Image img = ImageIO.read(inputStream); // 파일에서 이미지 읽기

        /*System.out.println("사진 가로 길이 : " + img.getWidth(null)); // 파일의 가로
        System.out.println("사진의 세로 길이 : " + img.getHeight(null)); // 파일의 세로*/

        // 파일의 길이 혹은 세로길이에 따라 if를 통해서 응용이 가능
        // 예를 들어 파일의 가로 해상도가 1000이 넘을 경우 1000으로 리사이즈

        int oldImageWidth = img.getWidth(null);
        int oldImageHeight = img.getHeight(null);
        int newWidth; // 리사이즈할 가로 길이
        int newHeight; // 리사이즈할 세로 길이

        if (oldImageWidth > oldImageHeight) {
            newWidth = 1000;
            newHeight = 1000 * oldImageHeight / oldImageWidth;
        } else {
            newWidth = 1000 * oldImageWidth / oldImageHeight;
            newHeight = 1000;
        }

        BufferedImage resizedImage = resize(inputStream, newWidth, newHeight); // 리사이즈 실행 메소드에 값을 넘겨줌.
        ImageIO.write(resizedImage, "jpg", new File("")); // 리사이즈된 파일, 포맷, 저장할 파일 경로

        System.out.println(oldImageHeight);
        System.out.println(oldImageWidth);
        System.out.println(newHeight);
        System.out.println(newWidth);
    }

    // 리사이즈 실행 메소드
    public static BufferedImage resize(InputStream inputStream, int width, int height) throws IOException {
        BufferedImage inputImage = ImageIO.read(inputStream); // 받은 이미지 읽기

        BufferedImage outputImage = new BufferedImage(width, height, inputImage.getType()); // 입력받은 리사이즈 길이와 높이

        Graphics2D graphics2D = outputImage.createGraphics();
        graphics2D.drawImage(inputImage, 0, 0, width, height, null); // 그리기
        graphics2D.dispose(); // 자원해제

        return outputImage;
    }
}