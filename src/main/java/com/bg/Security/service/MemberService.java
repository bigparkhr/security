package com.bg.Security.service;

import com.bg.Security.entity.Member;
import com.bg.Security.enums.MemberGroup;
import com.bg.Security.exception.CSecurityFullException;
import com.bg.Security.lib.ComonCheck;
import com.bg.Security.model.member.MemberIdDupCheckResponse;
import com.bg.Security.model.member.MemberRequest;
import com.bg.Security.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MemberService implements UserDetailsService {
    private final MemberRepository memberRepository;
    private final PasswordEncoder passwordEncoder;

    public MemberIdDupCheckResponse getDupCheck (String memberId){
        MemberIdDupCheckResponse response = new MemberIdDupCheckResponse();
        response.setIsNew(isMemberIdDupCheck(memberId));
        return response;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Member member = memberRepository.findByUsername(username).orElseThrow(ClassCastException::new);

        return member;
    }

    public void createMember(MemberGroup memberGroup, MemberRequest request) {
        if (!ComonCheck.checkUsername(request.getUsername())) throw new CSecurityFullException(); // 유효한 아이디 형식이 아닙니다 전지
        if (!request.getPassword().equals(request.getPasswordRe())) throw new CSecurityFullException(); // 비밀번호가 일치하지 않습니다 던지기
        if (!isMemberIdDupCheck(request.getUsername())) throw new CSecurityFullException(); // 중복된 아이디가 존재합니다 던지기

        request.setPassword(passwordEncoder.encode(request.getPassword()));
        Member member = new Member.Builder(request, memberGroup).build();

        memberRepository.save(member);
    }

    private boolean isMemberIdDupCheck(String memberId){
        long dupCheck = memberRepository.countByMemberIdOrderByIdDesc(memberId);
        return dupCheck <= 0;
    }
}
