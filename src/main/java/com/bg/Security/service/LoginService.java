package com.bg.Security.service;


import com.bg.Security.configure.JwtTokenProvider;
import com.bg.Security.entity.Member;
import com.bg.Security.enums.MemberGroup;
import com.bg.Security.exception.CSecurityFullException;
import com.bg.Security.model.login.LoginRequest;
import com.bg.Security.model.login.LoginResponse;
import com.bg.Security.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LoginService {
    private final MemberRepository memberRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenProvider jwtTokenProvider;

    // 로그인 타입은 WEB or APP (WEB인 경우 토큰 유효시간 10시간, APP은 1년)
    public LoginResponse doLogin(MemberGroup memberGroup, LoginRequest loginRequest, String loginType) {
        Member member= memberRepository.findByUsername(loginRequest.getUsername()).orElseThrow(CSecurityFullException::new);
        // 회원정보가 없습니다 던지기

        if (!member.getMemberGroup().equals(memberGroup)) throw new CSecurityFullException();
        // 일반회원이 최고관리자용으로 로그인하려거나 이런 경우이므로 메세지는 회원정보가 없습니다로 일단 던짐.
        if (!passwordEncoder.matches(loginRequest.getPassword(), member.getPassword())) throw new CSecurityFullException();
        // 비밀번호가 일치하지 않습니다를 던짐.

        String token = jwtTokenProvider.createToken(String.valueOf(member.getUsername()), member.getMemberGroup().toString(), loginType);

        return new LoginResponse.Builder(token, member.getUsername()).build();
    }
}
