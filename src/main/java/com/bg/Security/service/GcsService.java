package com.bg.Security.service;

import com.bg.Security.model.ImageRequest;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.IOException;
import java.io.InputStream;

@Service
public class GcsService {

    @Value("${spring.cloud.gcp.storage.bucket}")
    private String bucketName;

    public void uploadObject(ImageRequest imageRequest) throws IOException {
        String keyFileName = "classpath:idyllic-now-413600-0b292e8cefaa.json";
        InputStream keyFile = ResourceUtils.getURL(keyFileName).openStream();

        Storage storage = StorageOptions.newBuilder()
                .setCredentials(GoogleCredentials.fromStream(keyFile))
                .build()
                .getService();

        BlobInfo blobInfo = BlobInfo.newBuilder(bucketName, imageRequest.getFile().getOriginalFilename())
                .setContentType(imageRequest.getFile().getContentType()).build();

        Blob blob = storage.create(blobInfo, imageRequest.getFile().getInputStream());
    }
}