package com.bg.Security.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
