package com.bg.Security.model;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

@Data
public class ImageRequest {
    private String name;
    private MultipartFile file;
}