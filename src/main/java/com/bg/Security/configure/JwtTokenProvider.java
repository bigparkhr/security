package com.bg.Security.configure;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import jakarta.annotation.PostConstruct;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import java.util.Base64;
import java.util.Date;

@Component
@RequiredArgsConstructor
public class JwtTokenProvider {
    private final UserDetailsService userDetailsService;

    @Value("${spring.jwt.secret")
    private String secretKey;

    @PostConstruct
    protected void init() { // 먼저 키를 암호화
        secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes());
    }

    public String createToken(String username, String role, String type) {
        Claims claims = Jwts.claims().setSubject(username); // 클레임은 JWT에 포함될 정보를 나타냅니다. 여기서는 클레임의 주제(subject)를 사용자 이름으로 설정
        claims.put("role", role); // 사용자의 역할
        Date now = new Date(); // 토큰의 발급 시간으로 설정

        // 토큰 유효시간
        // 1000 밀리세컨드 = 1초
        // 기본을 10시간 유효하게 설정해줌 왜냐하면 아침에 출근해서 로그인하고 점심먹고 퇴근하면 대충 10시간이니깐
        // 앱용 토큰 같은 경우 유효시간 1년으로 설정해줌. 앱에서 아침마다 로그인하라고 하면 싫으니깐
        long tokenValidMillisecond = 1000L * 60 * 60 * 10; // 10 hour
        if (type.equals("APP")) tokenValidMillisecond = 1000L * 60 * 60 * 24 * 365; // 1년 웹 토큰은 쿠키도 시간을 동일하게 맞춤

        // 토큰 생성해서 리턴
        // jwt 사이트 참고
        // 유효시간도 넣어줌. 생성요청한 시간 ~ 현재 + 위에서 설정된 유효초 만큼.
        return Jwts.builder() // 토큰을 찍어내는 클래스
                .setClaims(claims) // 나야라고 명시
                .setIssuedAt(now) // 언제 발급됐는지
                .setExpiration(new Date(now.getTime() + tokenValidMillisecond)) // 언제 얘가 파기되는지 (만료일)
                .signWith(SignatureAlgorithm.HS256, secretKey) // 사인이 없으면 위조하기 때문에 시크릿키를 준비
                .compact(); // 토큰이 작기 때문에 콤팩트하게 작업

        // 토큰은 스트링으로 생김
    }

    // 토큰을 분석하여 인증정보를 가져옴
    public Authentication getAuthentication(String token) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(getUsername(token));
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

    public String getUsername(String token) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getSubject();
    }

    public String resolveToken(HttpServletRequest request) {
        return request.getHeader(HttpHeaders.AUTHORIZATION);
    }

    public boolean validateToken(String jwtToken) {
        try {
            Jws<Claims> claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(jwtToken);
            return !claims.getBody().getExpiration().before(new Date());
        } catch (Exception e) {
            return false;
        }
    }
}
