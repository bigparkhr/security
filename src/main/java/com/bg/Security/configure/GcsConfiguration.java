package com.bg.Security.configure;

import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class GcsConfiguration {
    @Bean
    public Storage storage() {
        return StorageOptions.getDefaultInstance().getService();
    }
}
