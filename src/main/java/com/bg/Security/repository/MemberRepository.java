package com.bg.Security.repository;

import com.bg.Security.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
public interface MemberRepository extends JpaRepository<Member, Long> {
    long countByMemberIdOrderByIdDesc(String memberId);
    Optional<Member> findByUsername(String username);
}
