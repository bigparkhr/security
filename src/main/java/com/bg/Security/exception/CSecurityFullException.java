package com.bg.Security.exception;

public class CSecurityFullException extends RuntimeException {
    public CSecurityFullException(String msg, Throwable t) {
        super(msg, t);
    }
    public CSecurityFullException(String msg) {
        super(msg);
    }

    public CSecurityFullException() {

    }
}
