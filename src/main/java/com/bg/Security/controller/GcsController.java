package com.bg.Security.controller;

import com.bg.Security.model.CommonResult;
import com.bg.Security.model.ImageRequest;
import com.bg.Security.service.GcsService;
import com.bg.Security.service.response.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/images")
public class GcsController {
    private final GcsService gcsService;

    @PostMapping("/api/upload")
    public CommonResult imageUpload(ImageRequest request) throws IOException {
        gcsService.uploadObject(request);

        return ResponseService.getSuccessResult();
    }
}