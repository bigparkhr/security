package com.bg.Security.controller;

import com.bg.Security.exception.CSecurityFullException;
import com.bg.Security.model.CommonResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/exception")
public class ExceptionController {

    @GetMapping("/access-denied")
    public CommonResult accessDeniedException() {
        throw new CSecurityFullException();
    }

    @GetMapping("/entry-point")
    public CommonResult entryPointException() {
        throw new CSecurityFullException();
    }
}
