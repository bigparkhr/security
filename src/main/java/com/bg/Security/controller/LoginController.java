package com.bg.Security.controller;


import com.bg.Security.enums.MemberGroup;
import com.bg.Security.model.SingleResult;
import com.bg.Security.model.login.LoginRequest;
import com.bg.Security.model.login.LoginResponse;
import com.bg.Security.service.LoginService;
import com.bg.Security.service.response.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/login")
public class LoginController {
    private final LoginService loginService;

    @PostMapping("/web/admin")
    @Operation(summary = "로그인 기능")
    public SingleResult<LoginResponse> doLoginAdmin(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberGroup.ROLE_MEMBER, loginRequest, "APP"));
    }

    @PostMapping("/app/user")
    public SingleResult<LoginResponse> doLoginUser(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberGroup.ROLE_CEO, loginRequest, "APP"));
    }
}
