package com.bg.Security.controller;

import com.bg.Security.model.CommonResult;
import com.bg.Security.service.TempService;
import com.bg.Security.service.response.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/temp")
public class TempController {
    private final TempService tempService;

    @PostMapping("/file-upload")
    public CommonResult setImageFile(@RequestParam("imageFile")MultipartFile imageFile) throws IOException {
        tempService.setImageFile(imageFile);
        return ResponseService.getSuccessResult();
    }
}